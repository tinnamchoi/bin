#!/bin/bash

FILE=$HOME/Documents/sleep.txt

[ "$1" == cat ] && cat $FILE && exit
[ "$1" == vim ] && vim $FILE && exit

date +%Y%m%d-%H%M%S >> $FILE
START=`date +%s`

echo "Press Enter to stop recording..."
read

date +%Y%m%d-%H%M%S >> $FILE
echo $((`date +%s` - $START)) >> $FILE

echo "Enter your note: "
read NOTE
echo "$NOTE" >> $FILE

echo "" >> $FILE

