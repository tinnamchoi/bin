#!/bin/bash

cd /mnt/canon/0_Files/Miscellaneous/Repositories/poem;

st -e sh -c '
vim poem.txt;
git add poem.txt;
git commit -m `date +"%Y%m%d-%H%M%S"`;
git push;
'
