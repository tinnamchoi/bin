#!/bin/bash

st -e sh -c '
xrandr --output HDMI-1 --left-of eDP-1 --auto;
xrandr --output HDMI-1 --pos 500x-1080;
echo Press Enter to stop output...;
read;
xrandr --output HDMI-1 --off
'
