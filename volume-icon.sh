#!/bin/bash

MUTE=$(pamixer --get-mute)
ICON=" "
[ "$MUTE" == true ] && ICON=" "

echo -n "$ICON"
pamixer --get-volume

